FROM alpine:3.7

ARG VCS_REF

ENV DOXYGEN_VERSION 1.8.13-r1

RUN apk update

RUN apk --update add graphviz

RUN apk --update add doxygen=${DOXYGEN_VERSION}

RUN apk --no-cache add msttcorefonts-installer fontconfig && \
    update-ms-fonts && \
    fc-cache -f

VOLUME /project

WORKDIR /project

CMD ["doxygen"]
